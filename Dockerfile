FROM openjdk

RUN DEBIAN_FRONTEND=noninteractive \
    ln -fs /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install \
      wget \
      unzip && \
    apt-get clean autoclean && \
    apt-get autoremove && \
    rm -rf /var/lib/{apt,dpkg,cache,log}

RUN mkdir -p /data/minecraft && cd /data/minecraft && \
    wget -O minecraft.zip https://minecraft.curseforge.com/projects/sevtech-ages/files/2560178/download && \
    unzip minecraft.zip && \
    rm minecraft.zip

RUN chmod +x /data/minecraft/Install.sh && \
    chmod +x /data/minecraft/ServerStart.sh

RUN cd /data/minecraft && \
    /data/minecraft/Install.sh

RUN echo "eula=true" > /data/minecraft/eula.txt
COPY ops.json /data/minecraft/ops.json

EXPOSE 25565 25565/udp
VOLUME ["/data/minecraft/world"]

WORKDIR /data/minecraft
ENTRYPOINT ["bash", "ServerStart.sh"]
